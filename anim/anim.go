package anim

import (
	"bitbucket.org/beulard/ice/spritesheet"
	sf "bitbucket.org/krepa098/gosfml2"
)

//	The exported Anim type is an int that is used as an index in the anims array (anims.go)
type Anim int

//	This is the true structure of an animation
type anim struct {
	frames       []*sf.Sprite
	currentframe uint
	frametime    float64
	currenttime  float64
	paused       bool
}

//	Load an animation from a sprite sheet and some parameters
func (index Anim) Load(sheet spritesheet.SpriteSheet, frames []sf.Vector2u, frametime float64) {
	self := animarray[index]
	self.frames = make([]*sf.Sprite, len(frames))
	self.frametime = frametime
	for i := range frames {
		self.frames[i] = sheet.GetSprite(frames[i].X, frames[i].Y)
	}
}

//	Update the animation : play it when it isn't paused
func (index Anim) Update(deltatime float64) {
	self := animarray[index]
	if !self.paused {

		self.currenttime += deltatime
		if self.currenttime >= self.frametime {
			self.currentframe++
			if self.currentframe >= (uint)(len(self.frames)) {
				self.currentframe = 0
			}
			self.currenttime = 0
		}
	}
}

//	Makes the animation play
func (index Anim) Play() {
	animarray[index].paused = false
}

//	Pauses the animation
func (index Anim) Pause() {
	animarray[index].paused = true
}

//	Is the anim paused?
func (index Anim) IsPaused() bool {
	return animarray[index].paused
}

//	Sets the origin point of the animation : refer to the SFML description of it
func (index Anim) SetOrigin(orig sf.Vector2f) {
	self := animarray[index]
	for i := range self.frames {
		self.frames[i].SetOrigin(orig)
	}
}

func (index Anim) GetOrigin() sf.Vector2f {
	return animarray[index].frames[0].GetOrigin()
}

func (index Anim) Rotate(angle float32) {
	self := animarray[index]
	for i := range self.frames {
		self.frames[i].Rotate(angle)
	}
}

func (index Anim) SetRotation(angle float32) {
	self := animarray[index]
	for i := range self.frames {
		self.frames[i].SetRotation(angle)
	}
}

func (index Anim) SetPos(x, y int) {
	self := animarray[index]
	for i := range self.frames {
		self.frames[i].SetPosition(sf.Vector2f{float32(x), float32(y)})
	}
}

func (index Anim) GetSize() sf.Vector2f {
	self := animarray[index]
	if len(self.frames) != 0 {
		return sf.Vector2f{float32(self.frames[0].GetTextureRect().Width), float32(self.frames[0].GetTextureRect().Height)}
	}
	return sf.Vector2f{0, 0}
}

func (index Anim) Draw(target sf.RenderTarget, states sf.RenderStates) {
	self := animarray[index]
	target.Draw(self.frames[self.currentframe], states)
}
