package anim

var (
	animarray []*anim
)

//	 Creates a new animation
func NewAnim() Anim {
	animarray = append(animarray, &anim{})
	return Anim(len(animarray) - 1)
}
