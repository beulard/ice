package physics

import (
	//	"fmt"
	"math"
)

//	Axis-aligned bounding box
type AABB struct {
	//	Sides of the box
	Left, Top, Width, Height float32
}

//type Collider interface {
//	CheckCollision(object Collider) bool
//	GetCollisionVector(object Collider) (float32, float32)
//}

func NewAABB() *AABB {
	return &AABB{}
}

func (self *AABB) CheckCollision(obj *AABB) bool {
	if self.Left+self.Width < obj.Left {
		return false
	}
	if self.Left > obj.Left+obj.Width {
		return false
	}
	if self.Top > obj.Top+obj.Height {
		return false
	}
	if self.Top+self.Height < obj.Top {
		return false
	}
	return true
}

func (self *AABB) GetCollisionVector(obj *AABB) (float32, float32) {
	center1x, center1y := self.Left+self.Width/2.0, self.Top+self.Height/2.0
	center2x, center2y := obj.Left+obj.Width/2.0, obj.Top+obj.Height/2.0
	var colvecx, colvecy float32
	if center1x < center2x {
		colvecx = obj.Left - self.Left - self.Width
	} else {
		colvecx = -(self.Left - obj.Left - obj.Width)
	}
	if center1y < center2y {
		colvecy = obj.Top - self.Top - self.Height
	} else {
		colvecy = -(self.Top - obj.Top - obj.Height)
	}
	if math.Abs(float64(colvecy)) < math.Abs(float64(colvecx)) {
		colvecx = 0
	} else if math.Abs(float64(colvecx)) < math.Abs(float64(colvecy)) {
		colvecy = 0
	}
	//	} else {
	// colvecx = 0
	// colvecy = 0
	// }
	return colvecx, colvecy
}
