package main

import (
	"bitbucket.org/beulard/ice/anim"
	"bitbucket.org/beulard/ice/entity"
	"bitbucket.org/beulard/ice/game"
	"bitbucket.org/beulard/ice/game/tilemap"
	"bitbucket.org/beulard/ice/inputs"
	"bitbucket.org/beulard/ice/spritesheet"

	"bitbucket.org/kardianos/osext"
	sf "bitbucket.org/krepa098/gosfml2"
	"fmt"
	"os"
	"strconv"
	"time"
)

func main() {
	win := sf.NewRenderWindow(sf.VideoMode{32 * 28, 32 * 30, 32}, "gosfml", sf.StyleDefault, sf.ContextSettingsDefault())

	exedir, _ := osext.ExecutableFolder()
	os.Chdir(exedir)

	//	Let's have a 15 frames history of inputs from the player to smoothe the game up a bit
	inputHistory := make([]inputs.Inputs, 15)

	//	Spritesheet
	charsheet := spritesheet.NewSpriteSheet()
	charsheet.Load("res/characters.png", 32, 32)

	//	Animation
	pacmanim := anim.NewAnim()
	pacmanim.Load(
		charsheet,
		[]sf.Vector2u{sf.Vector2u{0, 0}, sf.Vector2u{1, 0}, sf.Vector2u{2, 0}, sf.Vector2u{3, 0}, sf.Vector2u{2, 0}, sf.Vector2u{1, 0}},
		0.05)

	redanim := anim.NewAnim()
	redanim.Load(charsheet,
		[]sf.Vector2u{sf.Vector2u{0, 1}},
		9999)

	//	Character
	pacman := game.NewPacman()
	pacman.AddAnim(pacmanim)
	pacman.MakeCenterOrigin()
	pacman.SetTopLeft(96, 32)
	pacman.SetSpeed(3.0)
	pacman.SetDirection(entity.Right)

	red := game.NewGhost()
	red.AddAnim(redanim)
	red.MakeCenterOrigin()
	red.SetTopLeft(32, 32)
	red.SetSpeed(2.0)
	red.SetDirection(entity.Left)
	//	FIXME faut pas rotate le sprite faut le flip :<

	//	Tilemap
	tilemap := tilemap.NewTileMap()
	tilemap.Load("res/map.txt")

	running := true

	currenttime, lastframetime := time.Now(), time.Now()

	font, _ := sf.NewFontFromFile("res/Andika-R.ttf")
	frameratetext := sf.NewText(font)
	frameratetext.SetCharacterSize(22)

	win.SetFramerateLimit(60)

	for running {
		for event := win.PollEvent(); event != nil; event = win.PollEvent() {
			switch ev := event.(type) {
			case sf.EventClosed:
				running = false
			case sf.EventKeyPressed:
				if ev.Code == sf.KeyEscape {
					running = false
				}
			}
		}

		currenttime = time.Now()
		deltatime := currenttime.Sub(lastframetime).Seconds()

		//	Handles inputs and the input history
		playerInputs := game.GetPlayerInputs()
		inputHistory[0] = *playerInputs
		game.UpdateHistory(inputHistory)

		pacman.Update(deltatime)
		red.Update(deltatime)
		game.ControlPacman(inputHistory, pacman, tilemap)
		pacx, pacy := pacman.CurrentTile()
		game.ControlRed(pacx, pacy, red, tilemap)
		fmt.Print()

		frameratetext.SetString("fps : " + strconv.Itoa(int(1.0/deltatime)))

		win.Clear(sf.Color{0, 0, 0, 255})

		win.Draw(tilemap, sf.RenderStatesDefault())
		win.Draw(pacman, sf.RenderStatesDefault())
		win.Draw(red, sf.RenderStatesDefault())
		win.Draw(frameratetext, sf.RenderStatesDefault())

		win.Display()
		lastframetime = currenttime
	}
}
