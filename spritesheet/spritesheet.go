package spritesheet

import (
	sf "bitbucket.org/krepa098/gosfml2"
	"fmt"
)

type spritesheet struct {
	tex *sf.Texture
	//	Width and height of each sprite
	w, h uint
}

type SpriteSheet interface {
	Load(name string, w, h uint)
	GetSprite(x, y uint) *sf.Sprite
}

///////////

func (self *spritesheet) Load(name string, w, h uint) {
	texture, err := sf.NewTextureFromFile(name, nil)
	if err != nil {
		fmt.Errorf("%s", err)
	}
	self.tex = texture
	self.w, self.h = w, h
}

func (self *spritesheet) GetSprite(x, y uint) *sf.Sprite {
	sprite := sf.NewSprite(self.tex)
	sprite.SetTextureRect(sf.IntRect{(int)(x * self.w), (int)(y * self.h), (int)(self.w), (int)(self.h)})
	return sprite
}

func NewSpriteSheet() SpriteSheet {
	return &spritesheet{nil, 0, 0}
}
