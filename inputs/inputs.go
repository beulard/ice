package inputs

type Inputs struct {
	bitset uint
}

func (self *Inputs) Has(pos uint) bool {
	return (self.bitset & (1 << pos)) != 0
}

func (self *Inputs) Set(pos uint, val bool) {
	if self.Has(pos) != val {
		self.bitset |= 1 << pos
	}
}
