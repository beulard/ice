package game

import (
	"bitbucket.org/beulard/ice/anim"
	"bitbucket.org/beulard/ice/entity"
	sf "bitbucket.org/krepa098/gosfml2"
	"math"
)

//	Character types
type CharacterType int

const (
	Pacman CharacterType = iota
	Ghost
)

type character struct {
	//	Array of animations
	anims []anim.Anim
	//	Current angle of the character
	angle float32
	//	Index of the current animation
	currentanim uint
	//	Physics properties
	velx, vely float32
	speed      float32
	//	Current tile
	currenttilex int
	currenttiley int
	//	Distance to the last tile the character was in
	disttolast int
	//	Is the character on the center of a tile ?
	ontile bool
	//	Type of character
	chartype CharacterType
	//	Current direction of the character
	dir entity.Direction
	//	Is the character in a tunnel
	intunnel bool
	//	Position of the tile the player will be in when he walks out of the tunnel
	tunnelendpos int
}

type Character interface {
	entity.Entity
	entity.Controllable
}

func NewPacman() *character {
	return &character{chartype: Pacman}
}

func NewGhost() *character {
	return &character{chartype: Ghost}
}

func (self *character) SetSpeed(speed float32) {
	self.speed = speed
}

func (self *character) SetDirection(dir entity.Direction) {
	//	If the character is in a tunnel, we disable directions
	if !self.intunnel {
		self.dir = dir
		switch self.chartype {
		case Pacman:
			switch dir {
			case entity.Up:
				self.SetRotation(-90)
				self.velx = 0.0
				self.vely = -self.speed
			case entity.Left:
				self.SetRotation(-180)
				self.velx = -self.speed
				self.vely = 0.0
			case entity.Right:
				self.SetRotation(0)
				self.velx = self.speed
				self.vely = 0.0
			case entity.Down:
				self.SetRotation(90)
				self.velx = 0.0
				self.vely = self.speed
			}
		case Ghost:
			switch dir {
			case entity.Up:
				self.velx = 0.0
				self.vely = -self.speed
			case entity.Left:
				self.SetRotation(-180)
				self.velx = -self.speed
				self.vely = 0.0
			case entity.Right:
				self.SetRotation(0)
				self.velx = self.speed
				self.vely = 0.0
			case entity.Down:
				self.velx = 0.0
				self.vely = self.speed
			}
		}
	}
}

func (self *character) TurnAround() {
	switch self.dir {
	case entity.Left:
		self.SetDirection(entity.Right)
		self.currenttilex--
	case entity.Right:
		self.SetDirection(entity.Left)
		self.currenttilex++
	case entity.Up:
		self.SetDirection(entity.Down)
		self.currenttiley--
	case entity.Down:
		self.SetDirection(entity.Up)
		self.currenttiley++
	}
	self.disttolast = 32 - self.disttolast
}

func (self *character) GetDirection() entity.Direction {
	return self.dir
}

func (self *character) AddAnim(anim anim.Anim) {
	self.anims = append(self.anims, anim)
}

func (self *character) SetAnim(animindex uint) {
	if animindex < uint(len(self.anims)) {
		self.currentanim = animindex
	}
}

func (self *character) PlayAnim() {
	self.anims[self.currentanim].Play()
}

func (self *character) PauseAnim() {
	self.anims[self.currentanim].Pause()
}

func (self *character) SetPos(x, y int) {
	for i := range self.anims {
		self.anims[i].SetPos(x, y)
	}
	self.currenttilex = int(x / 32)
	self.currenttiley = int(y / 32)
}

func (self *character) SetTopLeft(x, y int) {
	orig := sf.Vector2i{0, 0}
	if len(self.anims) != 0 {
		orig = sf.Vector2i{int(self.anims[0].GetOrigin().X), int(self.anims[0].GetOrigin().Y)}
	}

	for i := range self.anims {
		self.anims[i].SetPos(x+orig.X, y+orig.Y)
	}
	self.currenttilex = int(x / 32)
	self.currenttiley = int(y / 32)
}

func (self *character) SetOrigin(x, y float32) {
	for i := range self.anims {
		self.anims[i].SetOrigin(sf.Vector2f{x, y})
	}
}

func (self *character) MakeCenterOrigin() {
	for i := range self.anims {
		self.anims[i].SetOrigin(sf.Vector2f{self.anims[i].GetSize().X / 2, self.anims[i].GetSize().Y / 2})
	}
}

func (self *character) Rotate(angle float32) {
	for i := range self.anims {
		self.anims[i].Rotate(angle)
	}
}

func (self *character) SetRotation(angle float32) {
	for i := range self.anims {
		self.anims[i].SetRotation(angle)
	}
}

func (self *character) Stop() {
	self.velx = 0
	self.vely = 0
}

func (self *character) GoThroughTunnel(endpos int) {
	self.intunnel = true
	self.tunnelendpos = endpos
}

func (self *character) Update(deltatime float64) {
	//	Update some physics stuff and check if the character has moved enough to be considered on a new tile
	self.disttolast += int(math.Abs(float64(self.velx)))
	self.disttolast += int(math.Abs(float64(self.vely)))

	//	velx and vely would equal 0 if the pacman is stopped aka stuck on a wall
	if self.velx == 0 && self.vely == 0 {
		self.disttolast = 0
	}

	if !self.intunnel {
		if self.disttolast >= 32 {
			//	The character has arrived on a new tile
			self.ontile = true
			self.disttolast = self.disttolast - 32

			switch self.dir {
			case entity.Left:
				self.currenttilex--
			case entity.Right:
				self.currenttilex++
			case entity.Up:
				self.currenttiley--
			case entity.Down:
				self.currenttiley++
			}
		} else if self.disttolast != 0 {
			self.ontile = false
		}
	} else {
		if self.disttolast >= 16 {
			if self.dir == entity.Left || self.dir == entity.Right {
				self.currenttilex = self.tunnelendpos
			} else {
				self.currenttiley = self.tunnelendpos
			}
			self.disttolast = -16
			self.intunnel = false
		}
	}

	orig := self.anims[0].GetOrigin()

	//	Here we set the position of the animated sprites according to the the current tile and the estimated distance to it,
	//	so that we can have a smooth movement of the characters even if the physics system cannot handle such precision
	switch self.dir {
	case entity.Left:
		self.anims[self.currentanim].SetPos(self.currenttilex*32+int(orig.X)-self.disttolast, self.currenttiley*32+int(orig.Y))
	case entity.Right:
		self.anims[self.currentanim].SetPos(self.currenttilex*32+int(orig.X)+self.disttolast, self.currenttiley*32+int(orig.Y))
	case entity.Up:
		self.anims[self.currentanim].SetPos(self.currenttilex*32+int(orig.X), self.currenttiley*32+int(orig.Y)-self.disttolast)
	case entity.Down:
		self.anims[self.currentanim].SetPos(self.currenttilex*32+int(orig.X), self.currenttiley*32+int(orig.Y)+self.disttolast)

	}
	self.anims[self.currentanim].Update(deltatime)
}

func (self *character) OnTileCenter() bool {
	return self.ontile
}

func (self *character) CurrentTile() (x, y int) {
	return self.currenttilex, self.currenttiley
}

//	'character' implements the sf.Drawer interface here
func (self *character) Draw(target sf.RenderTarget, states sf.RenderStates) {
	self.anims[self.currentanim].Draw(target, states)
}
