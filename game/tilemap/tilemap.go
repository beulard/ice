package tilemap

import (
	"bitbucket.org/beulard/ice/physics"
	"bitbucket.org/beulard/ice/spritesheet"
	sf "bitbucket.org/krepa098/gosfml2"
	"fmt"
	"io/ioutil"
)

type coordinates struct {
	x, y int
}

type Tilemap struct {
	colmap    [][]*physics.AABB
	spritemap [][]*sf.Sprite
	junctions []coordinates
	tunnels   []coordinates
	sheet     spritesheet.SpriteSheet
	szx, szy  int
}

func (self *Tilemap) Load(name string) {
	b, _ := ioutil.ReadFile(name)
	szx, szy := 0, 0
	for i := range b {
		if b[i] == '\n' {
			if szx == 0 {
				szx = i - 1
			}
			szy++
		}
	}
	szy++

	self.szx = szx
	self.szy = szy

	self.colmap = make([][]*physics.AABB, szx)

	for i := 0; i < szx; i++ {
		self.colmap[i] = make([]*physics.AABB, szy)
	}

	//	Create a collision map from the map file
	x, y := 0, 0
	for i := 0; i < len(b); {
		if b[i] != '\n' && b[i] != '\r' {
			if b[i] != ' ' {
				self.colmap[x][y] = &physics.AABB{Left: float32(x * 32), Top: float32(y * 32), Width: 32, Height: 32}
			} else {
				self.colmap[x][y] = nil
			}
			x++
			i++
		} else {
			y++
			x = 0
			for b[i] == '\n' || b[i] == '\r' {
				i++
			}
		}
	}

	//	Find the junctions on the map
	for i := range self.colmap {
		for j, tile := range self.colmap[i] {
			//	For each tile, we look if it is empty (meaning it is not a wall)
			//	If it is, we look if any tiles around it are empty
			//	If one tile is empty, we look if a perpendicular tile is empty
			//	If that last tile is empty, then the tile is a junction tile
			if tile == nil {
				emptyabove, emptyright, emptyleft, emptybelow := false, false, false, false
				if i != 0 {
					if self.colmap[i-1][j] == nil {
						emptyleft = true
					}
				}
				if j != 0 {
					if self.colmap[i][j-1] == nil {
						emptyabove = true
					}
				}
				if i != szx-1 {
					if self.colmap[i+1][j] == nil {
						emptyright = true
					}
				}
				if j != szy-1 {
					if self.colmap[i][j+1] == nil {
						emptybelow = true
					}
				}

				if emptyabove && (emptyright || emptyleft) {
					self.junctions = append(self.junctions, coordinates{i, j})
				} else if emptyright && (emptyabove || emptybelow) {
					self.junctions = append(self.junctions, coordinates{i, j})
				} else if emptybelow && (emptyright || emptyleft) {
					self.junctions = append(self.junctions, coordinates{i, j})
				} else if emptyleft && (emptyabove || emptybelow) {
					self.junctions = append(self.junctions, coordinates{i, j})
				}
			}
		}
	}

	//	Find the tunnels on the map
	for i := range self.colmap {
		if self.colmap[i][0] == nil {
			self.tunnels = append(self.tunnels, coordinates{i, 0})
		}
		if self.colmap[i][szy-1] == nil {
			self.tunnels = append(self.tunnels, coordinates{i, szy - 1})
		}
	}
	for j := 1; j < szy-1; j++ {
		if self.colmap[0][j] == nil {
			self.tunnels = append(self.tunnels, coordinates{0, j})
		}
		if self.colmap[szx-1][j] == nil {
			self.tunnels = append(self.tunnels, coordinates{szx - 1, j})
		}
	}

	//	Create a sprite map from the map file
	self.sheet = spritesheet.NewSpriteSheet()
	self.sheet.Load("res/map.png", 32, 32)

	self.spritemap = make([][]*sf.Sprite, szx)
	for i := 0; i < szx; i++ {
		self.spritemap[i] = make([]*sf.Sprite, szy)
	}
	x, y = 0, 0
	for i := 0; i < len(b); {
		if b[i] != '\n' && b[i] != '\r' {
			switch b[i] {
			case ' ':
				self.spritemap[x][y] = self.sheet.GetSprite(3, 1)
			case '0':
				self.spritemap[x][y] = self.sheet.GetSprite(0, 0)
			case '1':
				self.spritemap[x][y] = self.sheet.GetSprite(0, 1)
			case '2':
				self.spritemap[x][y] = self.sheet.GetSprite(0, 2)
			case '3':
				self.spritemap[x][y] = self.sheet.GetSprite(0, 3)
			case '4':
				self.spritemap[x][y] = self.sheet.GetSprite(1, 0)
			case '5':
				self.spritemap[x][y] = self.sheet.GetSprite(1, 1)
			case '6':
				self.spritemap[x][y] = self.sheet.GetSprite(1, 2)
			case '7':
				self.spritemap[x][y] = self.sheet.GetSprite(1, 3)
			case '8':
				self.spritemap[x][y] = self.sheet.GetSprite(2, 0)
			case '9':
				self.spritemap[x][y] = self.sheet.GetSprite(2, 1)
			case 'a':
				self.spritemap[x][y] = self.sheet.GetSprite(2, 2)
			case 'b':
				self.spritemap[x][y] = self.sheet.GetSprite(2, 3)
			case 'c':
				self.spritemap[x][y] = self.sheet.GetSprite(3, 0)
			case 'd':
				self.spritemap[x][y] = self.sheet.GetSprite(3, 1)
			case 'e':
				self.spritemap[x][y] = self.sheet.GetSprite(3, 2)
			case 'f':
				self.spritemap[x][y] = self.sheet.GetSprite(3, 3)
			case 'g':
				self.spritemap[x][y] = self.sheet.GetSprite(4, 0)
			case 'h':
				self.spritemap[x][y] = self.sheet.GetSprite(4, 1)
			case 'i':
				self.spritemap[x][y] = self.sheet.GetSprite(4, 2)
			case 'j':
				self.spritemap[x][y] = self.sheet.GetSprite(4, 3)
			case 'k':
				self.spritemap[x][y] = self.sheet.GetSprite(5, 0)
			case 'l':
				self.spritemap[x][y] = self.sheet.GetSprite(5, 1)
			case 'm':
				self.spritemap[x][y] = self.sheet.GetSprite(5, 2)
			case 'n':
				self.spritemap[x][y] = self.sheet.GetSprite(5, 3)
			default:
				fmt.Printf("Bad character : %q", b[i])
			}
			self.spritemap[x][y].SetPosition(sf.Vector2f{float32(x * 32), float32(y * 32)})
			x++
			i++
		} else {
			y++
			x = 0
			for b[i] == '\n' || b[i] == '\r' {
				i++
			}
		}
	}
}

func (self *Tilemap) SizeX() int {
	return self.szx
}

func (self *Tilemap) SizeY() int {
	return self.szy
}

//	Checks if the tile {x, y} is a junction
func (self *Tilemap) IsJunction(x, y int) bool {
	coords := coordinates{x: x, y: y}
	for _, junc := range self.junctions {
		if coords == junc {
			return true
		}
	}
	return false
}

//	Checks if the tile {x, y} is a tunnel
func (self *Tilemap) IsTunnel(x, y int) bool {
	coords := coordinates{x, y}
	for _, tunnel := range self.tunnels {
		if coords == tunnel {
			return true
		}
	}
	return false
}

//	Checks if the tile {x, y} is empty
func (self *Tilemap) IsWall(x, y int) bool {
	return self.colmap[x][y] != nil
}

//	sf.Drawer interface implementation
func (self *Tilemap) Draw(target sf.RenderTarget, states sf.RenderStates) {
	for i := range self.spritemap {
		for j := range self.spritemap[i] {
			target.Draw(self.spritemap[i][j], states)
		}
	}
}

func NewTileMap() *Tilemap {
	return &Tilemap{}
}
