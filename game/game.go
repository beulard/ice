package game

import (
	"bitbucket.org/beulard/ice/inputs"
	sf "bitbucket.org/krepa098/gosfml2"
)

//	Player inputs
const (
	Left = iota
	Right
	Up
	Down
	Space
)

//	Returns an input.Input structure containing the inputs registered by sfml on this frame
func GetPlayerInputs() *inputs.Inputs {
	in := &inputs.Inputs{}
	if sf.KeyboardIsKeyPressed(sf.KeyW) || sf.KeyboardIsKeyPressed(sf.KeyUp) {
		in.Set(Up, true)
	} else {
		in.Set(Up, false)
	}
	if sf.KeyboardIsKeyPressed(sf.KeyA) || sf.KeyboardIsKeyPressed(sf.KeyLeft) {
		in.Set(Left, true)
	} else {
		in.Set(Left, false)
	}
	if sf.KeyboardIsKeyPressed(sf.KeyD) || sf.KeyboardIsKeyPressed(sf.KeyRight) {
		in.Set(Right, true)
	} else {
		in.Set(Right, false)
	}
	if sf.KeyboardIsKeyPressed(sf.KeyS) || sf.KeyboardIsKeyPressed(sf.KeyDown) {
		in.Set(Down, true)
	} else {
		in.Set(Down, false)
	}
	if sf.KeyboardIsKeyPressed(sf.KeySpace) {
		in.Set(Space, true)
	} else {
		in.Set(Space, false)
	}
	return in
}

//	Update the player input history, called each frame
func UpdateHistory(hist []inputs.Inputs) {
	//	Here we need to offset the history by one
	//	The most recent input being first in the array
	for i := len(hist) - 2; i >= 0; i-- {
		hist[i+1] = hist[i]
	}
}

//	This function returns the last input pressed by the player using the history
func WasPressedLast(hist []inputs.Inputs) int {
	for i := range hist {
		if hist[i].Has(Up) {
			return Up
		} else if hist[i].Has(Left) {
			return Left
		} else if hist[i].Has(Right) {
			return Right
		} else if hist[i].Has(Down) {
			return Down
		} else if hist[i].Has(Space) {
			return Space
		}
	}
	return -1
}
