package game

import (
	"bitbucket.org/beulard/ice/entity"
	"bitbucket.org/beulard/ice/game/tilemap"
	"bitbucket.org/beulard/ice/inputs"
)

//	The pacman controller need inputs, pacman's character struct and map info to set the direction
type PacmanController func([]inputs.Inputs, *character, *tilemap.Tilemap)

//	The ghost controller needs pacman's position, the ghost's character struct and map info
type GhostController func(int, int, *character, *tilemap.Tilemap)

func ControlPacman(inputHistory []inputs.Inputs, pacman *character, tilemap *tilemap.Tilemap) {
	//	This handles how pacman changes direction according to the player input
	if pacman.OnTileCenter() {
		curx, cury := pacman.CurrentTile()
		if tilemap.IsJunction(curx, cury) {
			//	Is pacman is on the center of a junction, the player can change his direction
			if WasPressedLast(inputHistory) == Left {
				pacman.SetDirection(entity.Left)
			} else if WasPressedLast(inputHistory) == Right {
				pacman.SetDirection(entity.Right)
			} else if WasPressedLast(inputHistory) == Down {
				pacman.SetDirection(entity.Down)
			} else if WasPressedLast(inputHistory) == Up {
				pacman.SetDirection(entity.Up)
			}
		}
	}

	//	This checks if pacman is running into a wall and stops him if he is
	//	Before checking for walls, we need to check if we are going through one of the map's tunnels
	curx, cury := pacman.CurrentTile()
	switch pacman.GetDirection() {
	case entity.Left:
		if curx == 0 {
			if tilemap.IsTunnel(curx, cury) {
				pacman.GoThroughTunnel(tilemap.SizeX() - 1)
			}
		} else if tilemap.IsWall(curx-1, cury) {
			pacman.Stop()
		}
	case entity.Right:
		if curx == tilemap.SizeX()-1 {
			if tilemap.IsTunnel(curx, cury) {
				pacman.GoThroughTunnel(0)
			}
		} else if tilemap.IsWall(curx+1, cury) {
			pacman.Stop()
		}
	case entity.Up:
		if cury == 0 {
			if tilemap.IsTunnel(curx, cury) {
				pacman.GoThroughTunnel(tilemap.SizeY() - 1)
			}
		} else if tilemap.IsWall(curx, cury-1) {
			pacman.Stop()
		}
	case entity.Down:
		if cury == tilemap.SizeY()-1 {
			if tilemap.IsTunnel(curx, cury) {
				pacman.GoThroughTunnel(0)
			}
		} else if tilemap.IsWall(curx, cury+1) {
			pacman.Stop()
		}
	}

	//	Make the player able to turn around
	if inputHistory[0].Has(Left) && pacman.GetDirection() == entity.Right {
		pacman.TurnAround()
	}
	if inputHistory[0].Has(Right) && pacman.GetDirection() == entity.Left {
		pacman.TurnAround()
	}
	if inputHistory[0].Has(Up) && pacman.GetDirection() == entity.Down {
		pacman.TurnAround()
	}
	if inputHistory[0].Has(Down) && pacman.GetDirection() == entity.Up {
		pacman.TurnAround()
	}
}

func ControlRed(pacx int, pacy int, red *character, tilemap *tilemap.Tilemap) {
	red.SetDirection(entity.Down)
	curx, cury := red.CurrentTile()

	if curx < pacx && !tilemap.IsWall(curx+1, cury) {
		red.SetDirection(entity.Right)
	} else if cury < pacy && !tilemap.IsWall(curx, cury+1) {
		red.SetDirection(entity.Down)
	} else if curx > pacx && !tilemap.IsWall(curx-1, cury) {
		red.SetDirection(entity.Left)
	} else if cury > pacy && !tilemap.IsWall(curx, cury-1) {
		red.SetDirection(entity.Up)
	}

	switch red.GetDirection() {
	case entity.Left:
		if curx == 0 {
			if tilemap.IsTunnel(curx, cury) {
				red.GoThroughTunnel(tilemap.SizeX() - 1)
			}
		} else if tilemap.IsWall(curx-1, cury) {
			red.Stop()
		}
	case entity.Right:
		if curx == tilemap.SizeX()-1 {
			if tilemap.IsTunnel(curx, cury) {
				red.GoThroughTunnel(0)
			}
		} else if tilemap.IsWall(curx+1, cury) {
			red.Stop()
		}
	case entity.Up:
		if cury == 0 {
			if tilemap.IsTunnel(curx, cury) {
				red.GoThroughTunnel(tilemap.SizeY() - 1)
			}
		} else if tilemap.IsWall(curx, cury-1) {
			red.Stop()
		}
	case entity.Down:
		if cury == tilemap.SizeY()-1 {
			if tilemap.IsTunnel(curx, cury) {
				red.GoThroughTunnel(0)
			}
		} else if tilemap.IsWall(curx, cury+1) {
			red.Stop()
		}
	}
}
