package entity

import (
	"bitbucket.org/beulard/ice/anim"
	sf "bitbucket.org/krepa098/gosfml2"
)

type Entity interface {
	AddAnim(anim anim.Anim)
	SetAnim(animindex uint)
	PlayAnim()
	PauseAnim()
	SetPos(x, y int)
	SetTopLeft(x, y int)
	SetOrigin(x, y float32)
	MakeCenterOrigin()
	Rotate(angle float32)
	SetRotation(angle float32)
	Update(deltatime float64)
	sf.Drawer
}
