package entity

type Direction int

//	Directions
const (
	Up = iota
	Left
	Down
	Right
)

type Controllable interface {
	SetDirection(dir Direction)
	GetDirection() Direction
	Stop()
	SetSpeed(speed float32)
	TurnAround()
}
